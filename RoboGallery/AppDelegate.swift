//
//  AppDelegate.swift
//  RoboGallery
//
//  Created by Mark Newton on 11/28/16.
//  Copyright © 2016 Mark Newton. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    lazy var coreDataStack = CoreDataStack(modelName: "RoboGallery")

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        guard let navController = self.window?.rootViewController as? UINavigationController,
            let galleryVC = navController.topViewController as? GalleryViewController else {
                return true
        }
        
        galleryVC.managedContext = coreDataStack.managedContext
        
        return true
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        self.coreDataStack.saveContext()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        self.coreDataStack.saveContext()
    }
}

