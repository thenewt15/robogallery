//
//  GalleryViewController.swift
//  RoboGallery
//
//  Created by Mark Newton on 11/28/16.
//  Copyright © 2016 Mark Newton. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire
import CoreData

class GalleryViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchBarContainerView: UIView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    var robots = [Robot]()
    var managedContext: NSManagedObjectContext!
    
    lazy var searchController: UISearchController = {
        let searchController = UISearchController(searchResultsController: nil)
        
        searchController.hidesNavigationBarDuringPresentation = true
        searchController.dimsBackgroundDuringPresentation = false
        
        searchController.searchBar.scopeButtonTitles = ["Set 1", "Set 2", "Set 3"]
        searchController.searchBar.delegate = self
        
        return searchController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.collectionView.register(UINib(nibName: String(describing: RobotImageCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: RobotImageCell.self))
        
        // add the searchBar to the UI
        self.searchBarContainerView.addSubview(self.searchController.searchBar)
        self.searchController.searchBar.sizeToFit()
        
        // use this view controller as the presenting view controller for the UISearchController.
        self.definesPresentationContext = true
        
        let request = NSFetchRequest<Robot>(entityName: "Robot")
        
        do {
            let robots = try managedContext.fetch(request)
            self.robots = robots
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    func fetchRobot(name: String, set: RobotSet) {
        
        self.spinner.startAnimating()
        
        RobotNetworkRequest.fetchRobot(name: name, set: set, completion: { [weak self] (image: UIImage?) in
            
            self?.spinner.stopAnimating()
            
            guard let image = image else {
                let alertVC = UIAlertController(title: NSLocalizedString("Uh oh!", comment: ""), message: NSLocalizedString("We weren't able to fetch a robot named \(name)", comment: ""), preferredStyle: .alert)
                let okAction = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: nil)
                alertVC.addAction(okAction)
                self?.present(alertVC, animated: true, completion: nil)
                
                return
            }
            
            if let strongSelf = self {
                let entity = NSEntityDescription.entity(forEntityName: "Robot", in: strongSelf.managedContext)!
                let robot = Robot(entity: entity, insertInto: strongSelf.managedContext)
                robot.avatarImageData = NSData(data: UIImagePNGRepresentation(image)!)
                robot.name = name
                
                do {
                    try strongSelf.managedContext.save()
                }
                catch let error as NSError {
                    print("Save error: \(error), description: \(error.userInfo)")
                }
                
                strongSelf.robots.append(robot)
                
                let firstIndexPath = IndexPath(item: 0, section: 0)
                strongSelf.collectionView.insertItems(at: [firstIndexPath])
            }
        })
    }
    
    func robotSet(forButtonIndex index: Int) -> RobotSet {
        switch index {
        case 0:
            return .set1
        case 1:
            return .set2
        case 2:
            return .set3
        default:
            assertionFailure("unable to determine a valid RobotSet")
            return .unknown
        }
    }
    
    // gets the robots in self.robots using the indexPath its displayed in
    func localRobot(forIndexPath indexPath: IndexPath) -> Robot {
        let robot = self.robots[self.robots.count - indexPath.row - 1]
        return robot
    }
}

extension GalleryViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.robots.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: RobotImageCell.self), for: indexPath) as? RobotImageCell {
            
            let robot = localRobot(forIndexPath: indexPath)
            
            cell.robotImageView.image = robot.avatarImage
            return cell
        }
        
        return UICollectionViewCell(frame: CGRect.zero)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let side = (self.collectionView.frame.width - 3) / 4
        return CGSize(width: side, height: side)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        collectionView.deselectItem(at: indexPath, animated: true)

        if let detailVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: String(describing: DetailViewController.self)) as? DetailViewController {
            
            detailVC.robot = localRobot(forIndexPath: indexPath)
            detailVC.delegate = self
            self.navigationController?.pushViewController(detailVC, animated: true)
        }
    }
}

extension GalleryViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        if let searchString = searchBar.text, !searchString.isEmpty {
            let set = robotSet(forButtonIndex: searchBar.selectedScopeButtonIndex)
            
            fetchRobot(name: searchString, set: set)
        }
        
        searchBar.text = ""
    }
}

extension GalleryViewController: DetailViewControllerDelegate {
    
    func detailViewController(_ controller: DetailViewController, didDeleteRobot robot: Robot) {
        
        _ = self.navigationController?.popViewController(animated: true)

        self.managedContext.delete(robot)
        self.robots = self.robots.filter { $0 != robot }
        
        do {
            try self.managedContext.save()
            self.collectionView.reloadData()
        } catch let error as NSError {
            print("Saving error: \(error), description: \(error.userInfo)")
        }
    }
}
