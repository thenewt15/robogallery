//
//  DetailViewController.swift
//  RoboGallery
//
//  Created by Mark Newton on 11/29/16.
//  Copyright © 2016 Mark Newton. All rights reserved.
//

import UIKit

protocol DetailViewControllerDelegate: class {
    func detailViewController(_ controller: DetailViewController, didDeleteRobot robot: Robot)
}

class DetailViewController: UIViewController {

    @IBOutlet weak var robotImageView: UIImageView!
    @IBOutlet weak var robotNameLabel: UILabel!
    
    var robot: Robot!
    weak var delegate: DetailViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assert(robot != nil, "a nonnull robot is required")
        
        self.robotImageView.image = robot.avatarImage
        let name = robot.name ?? "nameless"
        self.robotNameLabel.text = NSLocalizedString("Hi, I'm \(name)!", comment: "")
        
        self.navigationItem.title = NSLocalizedString("Robot Details", comment: "navigation bar title for Robot Details screen")
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.trash, target: self, action: #selector(deleteBarButtonTapped))
    }
    
    func deleteBarButtonTapped() {
        
        let robotTitle = robot.name ?? "this robot"
        let title = NSLocalizedString("Are you sure?", comment: "alert title shown after tapping delete button")
        let message = NSLocalizedString("Do you want to send \(robotTitle) to the rubbish pile?", comment: "alert message shown after tapping delete button")
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel",comment: ""), style: .cancel, handler: nil)
        let deleteAction = UIAlertAction(title: NSLocalizedString("Delete",comment: ""), style: .destructive, handler: { (action: UIAlertAction) -> Void in
            
            self.delegate?.detailViewController(self, didDeleteRobot: self.robot)
        })
        
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertVC.addAction(cancelAction)
        alertVC.addAction(deleteAction)
        
        present(alertVC, animated: true, completion: nil)
    }
}
