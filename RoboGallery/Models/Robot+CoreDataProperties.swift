//
//  Robot+CoreDataProperties.swift
//  RoboGallery
//
//  Created by Mark Newton on 11/29/16.
//  Copyright © 2016 Mark Newton. All rights reserved.
//

import Foundation
import CoreData

extension Robot {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Robot> {
        return NSFetchRequest<Robot>(entityName: "Robot");
    }

    @NSManaged public var name: String?
    @NSManaged public var avatarImageData: NSData?
}
