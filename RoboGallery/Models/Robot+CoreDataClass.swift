//
//  Robot+CoreDataClass.swift
//  RoboGallery
//
//  Created by Mark Newton on 11/29/16.
//  Copyright © 2016 Mark Newton. All rights reserved.
//

import Foundation
import UIKit
import CoreData

@objc(Robot)
public class Robot: NSManagedObject {

    var avatarImage: UIImage? {
        get {
            if let image = self.privateAvatarImage {
                return image
            } else if let data = self.avatarImageData as? Data {
                return UIImage(data: data)
            }
            
            return nil
        }
        
        set {
            self.privateAvatarImage = newValue
        }
    }
    
    private var privateAvatarImage: UIImage?
}
