//
//  RobotImageCell.swift
//  RoboGallery
//
//  Created by Mark Newton on 11/28/16.
//  Copyright © 2016 Mark Newton. All rights reserved.
//

import UIKit

class RobotImageCell: UICollectionViewCell {

    @IBOutlet weak var robotImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.robotImageView.image = nil
    }

}
