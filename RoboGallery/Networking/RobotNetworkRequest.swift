//
//  RobotNetworkRequest.swift
//  RoboGallery
//
//  Created by Mark Newton on 11/29/16.
//  Copyright © 2016 Mark Newton. All rights reserved.
//

import UIKit
import Alamofire

enum RobotSet: String {
    case unknown, set1, set2, set3
}

struct RobotNetworkRequest {
    
    static func fetchRobot(name: String, set: RobotSet? = nil, completion: @escaping (UIImage?) -> Void) {
        
        var urlString = "https://robohash.org/" + name
        if let set = set {
            urlString += "?set=\(set)"
        }
        
        // allows names to have spaces by properly encoding them.
        let urlEncodedString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? urlString
        
        Alamofire.request(urlEncodedString).responseImage { response in
            
            guard response.result.isSuccess else {
                print("Error while fetching robot image: \(response.result.error)")
                completion(nil)
                return
            }
            
            guard let image = response.result.value else {
                print("Malformed data received from service")
                completion(nil)
                return
            }
            
            completion(image)
        }
    }
}
